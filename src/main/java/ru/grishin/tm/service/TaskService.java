package ru.grishin.tm.service;

public interface TaskService {
    void create(String name, int id, int projectId);
    void update(String name, int id);
    void read(int projectId);
    void delete(int id);
    void clear(int projectId);
}
