package ru.grishin.tm.service;

import ru.grishin.tm.entity.Project;

import java.util.ArrayList;
import java.util.List;

public class ProjectServiceImpl implements ProjectService {
    private List<Project> projects = new ArrayList<>();

    @Override
    public void create(String name, int id) {
        Project project = new Project();
        project.setName(name);
        project.setId(id);
        projects.add(project);
    }

    @Override
    public void update(String name, int id) {
        for (int i = 0; i < projects.size(); i++) {
            if (projects.get(i).getId() == id) projects.get(i).setName(name);
        }
    }

    @Override
    public void read() {
        for (Project project : projects) {
            System.out.println(project.getId() + ". " + project.getName());
        }
    }

    @Override
    public void delete(int id) {
        for (int i = 0; i < projects.size(); i++) {
            if (projects.get(i).getId() == id) projects.remove(i);
        }
    }

    @Override
    public void clear() {
        projects.removeAll(projects);
    }


    public List<Project> getProjects() {
        return projects;
    }

    public void setProjects(ArrayList<Project> projects) {
        this.projects = projects;
    }
}
