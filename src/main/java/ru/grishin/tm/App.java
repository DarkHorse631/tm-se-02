package ru.grishin.tm;

import ru.grishin.tm.service.ProjectServiceImpl;
import ru.grishin.tm.service.TaskServiceImpl;
import ru.grishin.tm.view.View;

import java.util.Scanner;

public class App {
    public static void main(String[] args) {
        int idProject = 1;
        int idTask = 1;
        View view = new View();
        ProjectServiceImpl projectService = new ProjectServiceImpl();
        TaskServiceImpl taskService = new TaskServiceImpl();
        Scanner scanner = new Scanner(System.in);

        System.out.println("*** WELCOME TO TASK MANAGER ***");
        System.out.print("Enter \"help\" for show all commands: ");

        String command;
        int projectId;
        int taskId;
        do {
            command = scanner.nextLine();
            switch (command) {
                case ("h"):
                case ("help"):
                    view.showHelpList();
                    break;
                case ("cp"):
                case ("create-project"):
                    System.out.println("--Create project--");
                    System.out.print("Enter name: ");
                    String name = scanner.nextLine();
                    projectService.create(name, idProject++);
                    break;
                case ("up"):
                case ("update-project"):
                    System.out.println("--update project--");
                    System.out.print("Enter the project id: ");
                    projectId = Integer.parseInt(scanner.nextLine());
                    System.out.print("Enter new name: ");
                    name = scanner.nextLine();
                    projectService.update(name, projectId);
                    break;
                case ("sap"):
                case ("show-all-projects"):
                    System.out.println("--Show all projects--");
                    projectService.read();
                    break;
                case ("rp"):
                case ("remove-project"):
                    System.out.println("--Remove project--");
                    System.out.print("Enter the project id");
                    projectId = Integer.parseInt(scanner.nextLine());
                    projectService.delete(projectId);
                    break;
                case ("cpr"):
                case ("clear-project"):
                    System.out.println("--Remove all projects-");
                    projectService.clear();
                    idProject = 1;
                    break;
                case ("ct"):
                case ("create-task"):
                    System.out.println("--Create Task--");
                    System.out.print("Enter project id: ");
                    projectId = Integer.parseInt(scanner.nextLine());
                    System.out.println("Enter name task");
                    command = scanner.nextLine();
                    taskService.create(command, idTask++, projectId);
                    break;
                case ("ut"):
                case ("update-task"):
                    System.out.println("--Update Task--");
                    System.out.print("Enter the project id: ");
                    taskId = Integer.parseInt(scanner.nextLine());
                    System.out.println("Enter new name");
                    name = scanner.nextLine();
                    taskService.update(name, taskId);
                    break;
                case ("tl"):
                case ("task-list"):
                    System.out.println("--Task List--");
                    System.out.print("Enter project id: ");
                    projectId = Integer.parseInt(scanner.nextLine());
                    taskService.read(projectId);
                    break;
                case ("rt"):
                case ("remove-task"):
                    System.out.println("--Delete task--");
                    System.out.print("Enter the task ID: ");
                    taskId = Integer.parseInt(scanner.nextLine());
                    taskService.delete(taskId);
                    break;
                case ("cat"):
                case ("clear-all-tasks"):
                    System.out.println("--Clear task list--");
                    System.out.print("Enter ID of project");
                    projectId = Integer.parseInt(scanner.nextLine());
                    taskService.clear(projectId);
                    idTask = 1;
                    break;
                    default:
                        System.out.println("---Wrong command---");
            }
        } while (!command.equals("exit"));
    }
}