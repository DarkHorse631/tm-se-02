package ru.grishin.tm.view;

public class View {
    public void showHelpList(){
        System.out.println("help: Show all commands. " +
                "\n create-project (cp): Create new project." +
                "\n update-project (up): Update selected project." +
                "\n show-all-projects (sap): Show all projects." +
                "\n remove-project (rp): Remove selected project." +
                "\n clear-project (cpr): Remove all projects" +
                "\n" +
                "\n create-task- (ct): Create new task." +
                "\n update-task- (ut): Update selected project." +
                "\n task-list (tl): Show all tasks." +
                "\n remove-task (rt): Remove selected task." +
                "\n clear-all-tasks (cat): Remove all tasks." +
                "\n " +
                "\n exit: Exit from app.");
    }
}
